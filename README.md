## Présentation courte du projet
Flyer Voyager (ou Flyager) est un flyer permettant de retrouver sur une carte où le flyer à été (re)trouvé, emmené, raporter ... Et ainsi suivre le voyage des Hôtes de l'Hôtel.

## Note technique

### Quel dispositif technique utilise le projet ?
Ce projet utilise un QR code qui renvoie vers une page où la personne trouvant le flyer peut partager sa position.

L'ensemble des positions enregistrées sont ensuite représenté sur une carte.

### Quel matériel nécessite le projet ?

* L'usager à besoin d'un téléphone pour scanner le QRCode
* La page web est connectée à une base de donnée (CouchDB) pour enregistrer et afficher les coordonnées.

### A quelle fréquence le dispositif envoie des données

Le dispositif envoie des données quand l'usager valide sa position sur la page web.

## Notice d'installation

### Introduction

Pour fonctionner le dispositif nécessite :

* Un site WordPress (testé avec v5.7)
* Une base de données CouchDB accessible depuis le web

Le dispositif de présente sous la forme d'un plugin qui crée un endpoint pour l'API de la base de donnée CouchDB

### Déploiement

Premièrement, télécharger et décompresser le contenu de l'archive dans le dossier `wp-content/plugins/flyer-voyager-plugin`.


Il est nécessaire d'ajouter la clée d'authentification 'accès en lecture et en écriture de CouchDB dans le fichier `wp-config.php` de l'installation WordPress. Si ces variables ne sont pas définies, le navigateur affichera un message d'erreur : "Constantes non définies".

Pour ce faire, il faut ajouter le bloc de code suivant en dessous de `define( 'WP_DEBUG', false );` :

```
/**
 * Flyer-Voyager Plugin (fvp)
 * 
 * We define our reader and writer api key (Base64 basic auth key) for the flyer-voyager wp plugin
 */
define( 'FVP_DB_URL', 'data.hotelpasteur.fr');
define( 'FVP_DB_WRITER_KEY', 'd3JpdGVyOlQxUU55ZjNNQWVqZm81ZzY=');
define( 'FVP_DB_READER_KEY', 'cmVhZGVyOmdSZzdac3dGNEx4ZHhVdGs=');
```

### Utilisation

L'installation du plugin permet trois choses :



