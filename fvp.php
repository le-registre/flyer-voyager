<?php

/**
 * @package Hello_Dolly
 * @version 1.6
 */
/*
Plugin Name: Flyer Voyager Plugin
Plugin URI: http://wordpress.org/extend/plugins/hello-dolly/
Description: Register a custom api endpoint and add a security layer for flyer voyager coordinates posting and deleting (see README.md for documentation)
Author: Marin Esnault
Version: 1
Author URI: https://marineno.com/
*/

add_action('rest_api_init', function () {
    register_rest_route('flyer-voyager/v1', '/send-position/', array(
        'methods' => 'POST',
        'callback' => 'send_position',
        'permission_callback' => '__return_true',
    ));
    register_rest_route('flyer-voyager/v1', "/delete-position/(?P<id>[a-z0-9]+)", array(
        'methods' => 'DELETE',
        'callback' => 'delete_position',
        'permission_callback' => function () {
            return current_user_can('edit_others_posts');
        }
    ));
});

function send_position(WP_REST_Request $request)
{
    if (defined("FVP_DB_URL") && defined('FVP_DB_WRITER_KEY')) {
        $json = $request->get_json_params();
        date_default_timezone_set("Europe/Paris");
        $json['timestamp'] = date("Y-m-d H:i:s");
        // URL of the remote server
        $url = "https://" . constant("FVP_DB_URL") . "/flyer-voyager";
        // Auth key
        $key = constant('FVP_DB_WRITER_KEY');

        $header = array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . $key
        );
        $response = wp_remote_post(
            $url,
            array(
                'method'      => 'POST',
                'timeout'     => 45,
                'redirection' => 5,
                'httpversion' => '2',
                'headers'     => $header,
                'body'        => json_encode($json),
            )
        );

        if (is_wp_error($response)) {
            $error_message = $response->get_error_message();
            return $error_message;
        } else {
            return $response;
        }
    }
}
function delete_position($request)
{
    /* `https://<?php echo constant("FVP_DB_URL") ?>/flyer-voyager/${id}?rev=${rev}` */
    if (defined("FVP_DB_URL") && defined('FVP_DB_WRITER_KEY')) {

        // URL of the remote server
        $url = "https://" . constant("FVP_DB_URL") . "/flyer-voyager/" . $request["id"] . "?rev=" . $request["rev"];
        // Auth key
        $key = constant('FVP_DB_WRITER_KEY');

        $header = array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . $key
        );
        $response = wp_remote_post(
            $url,
            array(
                'method'      => 'DELETE',
                'httpversion' => '2',
                'headers'     => $header,
            )
        );

        if (is_wp_error($response)) {
            $error_message = $response->get_error_message();
            return $error_message;
        } else {
            return $response;
        }
    }
}
/**
 * Enqueue scripts and styles.
 */
function fvp_scripts()
{
}
//add_action('wp_enqueue_scripts', 'fvp_scripts');
/**
 * Conditionnally enqueue scripts and styles.
 */
/*
 <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js"></script>
<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css">
<!-- Promise polyfill script required to use Mapbox GL Geocoder in IE 11 -->
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
 */
function custom_shortcode_scripts()
{
    global $post;
    if (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'fvp_map')) {
        wp_enqueue_style('fvp', plugin_dir_url(__FILE__) . 'css/style.css', array(), '1.0', 'all');
    }
    if (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'fvp_geocoder')) {
        wp_enqueue_style('mapbox-gl-geocoder', 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css', null,  '4.5.1', 'all');
        wp_enqueue_script('mapbox-gl-geocoder', 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js', null, '4.5.1', false);
        wp_enqueue_style('fvp', plugin_dir_url(__FILE__) . 'css/style.css', array(), '1.0', 'all');
    }
}
add_action('wp_enqueue_scripts', 'custom_shortcode_scripts');

function enqueue_admin_script( $hook ) {
    wp_enqueue_script('fvp-settings', plugin_dir_url( __FILE__ ) . 'partials/settings.js', array(), '1.0.0');
    wp_localize_script( 'fvp-settings', 'wpApiSettings', array( 'root' => esc_url_raw( rest_url() ), 'nonce' => wp_create_nonce( 'wp_rest' ) ) );
}
add_action( 'admin_enqueue_scripts', 'enqueue_admin_script' );

/**
 * The [fvp] shortcode.
 *
 * Display the map
 *
 */
function fvp_map_shortcode()
{
    ob_start();
    include('partials/map.php');
    return ob_get_clean();
}

/**
 * The [fvp] shortcode.
 *
 * Display the geocoder
 * 
 * @param array  $atts    Shortcode attributes. Default empty.
 * @param string $tag     Shortcode tag (name). Default empty.
 * @return string Shortcode output.
 */
function fvp_geocoder_shortcode($atts = [], $tag = '')
{
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array) $atts, CASE_LOWER);

    // override default attributes with user attributes
    $wporg_atts = shortcode_atts(
        array(
            'redirection_slug' => '',
        ),
        $atts,
        $tag
    );
    ob_start();
    include('partials/geocoder.php');
    return ob_get_clean();
}

function fvp_shortcodes_init()
{
    add_shortcode('fvp_map', 'fvp_map_shortcode');
    add_shortcode('fvp_geocoder', 'fvp_geocoder_shortcode');

}

add_action('init', 'fvp_shortcodes_init');

// Menu page for editing point on the map

// In plugin main file
add_action('admin_menu', 'flyerVoyagerSettings');
/**
 * Registers our new menu item
 */
function flyerVoyagerSettings()
{
    // Create a top-level menu item.
    $hookname = add_menu_page(
        __('Flyer Voyager', 'fvp'),  // Page title
        __('Flyer Voyager', 'fvp'),  // Menu title
        'manage_options',                     // Capabilities
        'fvp_settings_page',              // Slug
        'fvp_settings_page_markup',       // Display callback
        'dashicons-site',                   // Icon
        66                                    // Priority/position. Just after 'Plugins'
    );
}

/**
 * Markup callback for the settings page
 */
function fvp_settings_page_markup()
{
    ob_start();
    include('partials/settings.php');
    ob_end_flush();
}
