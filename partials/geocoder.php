<?php
if (defined("FVP_DB_URL") && defined('FVP_DB_WRITER_KEY')) {
?>

<script src='https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.css' rel='stylesheet' />

    <section id="map-geocoder-container">
        <div id="map-geocoder"></div>
    </section>
    <section id="form">
        <form name="metadata">
            <div class="form-field">
                <label for="name">Nom du collectif :</label>
                <input type="text" name="name" id="name" required />
            </div>
            <div class="form-field">
                <label for="host">Structure d'accueil :</label>
                <input type="text" name="host" id="host" placeholder="(optionnel)"/>
            </div>
            <div class="form-field">
                <label for="email">Informations de contact :</label>
                <input type="email" id="email" size="30" placeholder="(optionnel)"/>
            </div>
            <div class="form-field">
                <label for="url">Site web :</label>
                <input type="url" name="url" id="url" placeholder="(optionnel)" size="30" />
            </div>
            <div class="form-field send-button">
                <button id="send">Envoyer ma position</button>
            </div>
        </form>
    </section>
    <script>
        // We create a position array which will store the user lat and lng
        let position = [];

        // Map part
        mapboxgl.accessToken = 'pk.eyJ1IjoiZW5vbWFyaW4iLCJhIjoiY2ttM3U2ZTl4MzdpYzMxbXd5aTlsZWh2ZyJ9.DW8emqXsSG1AtHmFZ6GuqQ';
        const mapGeo = new mapboxgl.Map({
            container: 'map-geocoder',
            style: 'mapbox://styles/enomarin/ckm4pd1mi0nuh17r1t6c19mzf/draft',
            center: [-1.6733523049179844, 48.11077868682573],
            zoom: 8
        });
        mapGeo.on('click', addMarker);
        // We create a marker variable to let the user select a place with the map or the geocoder
        let marker

        //We get currentPosition (work better with smartphone) and create a new marker on the map
        navigator.geolocation.getCurrentPosition(function(CurrentPosition) {
            position = [CurrentPosition.coords.longitude, CurrentPosition.coords.latitude];
            if (typeof marker !== "undefined") {
                marker.setLngLat(position)
            } else {
                //add a new marker
                marker = new mapboxgl.Marker().setLngLat(position).addTo(mapGeo);
            }

            toggleOn()
            //console.log("position on geoloc is : ", position);
        }, function(e) {
            //Your error handling here
        }, {
            enableHighAccuracy: true
        });

        function addMarker(e) {
            position = [e.lngLat.lng, e.lngLat.lat];
            if (typeof marker !== "undefined") {
                marker.setLngLat(position)
            } else {
                //add a new marker
                marker = new mapboxgl.Marker().setLngLat(position).addTo(mapGeo);
            }

            //console.log("position on map click is : ", position);
            toggleOn()
        }

        let geocoder = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            enableEventLogging: false,
            mapboxgl: mapboxgl,
            placeholder: "Rechercher un lieu",
            proximity: {
                longitude: -1.6733523049179844,
                latitude: 48.11077868682573
            },
            trackProximity: true
        });
        //console.log(geocoder.getProximity());
        geocoder.on('result', r => {
            position = r.result.center;
            if (typeof marker !== "undefined") {
                marker.setLngLat(position)
            } else {
                //add a new marker
                marker = new mapboxgl.Marker().setLngLat(position).addTo(mapGeo);
            }

            //console.log("position in geocoder is : ", position);
            toggleOn()
        })

        geocoder.on('loading', r => {
            toggleOff()
        })
        geocoder.on('clear', r => {
            toggleOff()
        })
        mapGeo.addControl(geocoder);
        // Button init
        let btnSend = document.getElementById("send");
        toggleOff()

        function toggleOn() {
            if (position.length == 2) {
                btnSend.disabled = false;
                btnSend.classList.remove("button-inactive");
                btnSend.classList.add("button-active");
            }
        }

        function toggleOff() {
            if (position.length != 2) {
                btnSend.disabled = true;
                btnSend.classList.remove("button-active");
                btnSend.classList.add("button-inactive");
            }
        }

        // form init
        let form = document.forms.metadata;
        form.addEventListener("submit", (event) => {
            event.preventDefault();
            sendPosition();
        });

        function sendPosition() {
            let json = {
                coord: position,
                name: form.name.value,
                host: form.host.value,
                email: form.email.value,
                url: form.url.value
            };
            //console.log(json);
            if (
                json.coord.length == 2 &&
                typeof json.coord[0] === "number" &&
                typeof json.coord[1] === "number"
            ) {
                fetch('https://le-registre.hotelpasteur.fr/wp-json/flyer-voyager/v1/send-position/', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(json),
                    })
                    .then((res) => {
                        if (res.ok) {
                            let redirection_slug = "<?php echo $wporg_atts['redirection_slug'] ?>";
                            if (redirection_slug === "") {
                                btnSend.innerHTML = "Position Envoyée !";
                            } else {
                                alert(
                                    " Position Envoyée !\nVous allez être redirigé vers la carte"
                                );
                                window.location.replace("/essaimage");
                            }
                        } else {
                            alert(
                                res.status +
                                ": Il y a eu un problème dans l'envoie de la position"
                            );
                        }
                    })
                    .catch(err => {
                        alert("Il y a eu un problème dans l'envoie de la position : \n" + err)
                    });
            } else {
                alert("Il y a eu un problème dans l'envoie de la position");
            }
        }
    </script>
<?php
} else {
?>
    <script>
        alert(
            "Il il y a un problème avec la page : Constantes non définies"
        );
    </script>
<?php
}
?>