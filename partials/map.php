<section id="map-container">
    <div id="map"></div>
</section>

<script src='https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.css' rel='stylesheet' />

<script>
    console.log(mapboxgl);
    mapboxgl.accessToken = 'pk.eyJ1IjoiZW5vbWFyaW4iLCJhIjoiY2ttM3U2ZTl4MzdpYzMxbXd5aTlsZWh2ZyJ9.DW8emqXsSG1AtHmFZ6GuqQ';

    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/enomarin/ckm4pd1mi0nuh17r1t6c19mzf/draft',
        zoom: 10,
        center: [-1.6733523049179844, 48.11077868682573]
    });
    <?php
    if (defined("FVP_DB_URL") && defined("FVP_DB_READER_KEY")) {
    ?>

        fetch('https://<?php echo constant("FVP_DB_URL") ?>/flyer-voyager/_all_docs?include_docs=true', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Basic <?php echo constant("FVP_DB_READER_KEY") ?>'
                },
            })
            .then(response => response.json())
            .then(data => data.rows.map(e => {
                if (e.doc.coord != undefined && e.doc.coord.length == 2 && !Number.isNaN(e.doc.coord[0]) && !Number.isNaN(e.doc.coord[1])) {
                    var el = document.createElement('div');
                    el.className = 'marker';
                    let marker = new mapboxgl.Marker(el)
                        .setLngLat(e.doc.coord)
                    if (e.doc.name || e.doc.host) {
                        let popup = new mapboxgl.Popup({
                            offset: 25
                        }).setHTML(`${e.doc.name ? `<div class="row"><h5 class="title">${e.doc.name}</h5></div>` : ''}
                                                    ${e.doc.host ? `<div class="row">
                                                        <img src="<?php echo plugin_dir_url(__DIR__) . 'images/essaimage.png'; ?>" alt="structure d'accueil"> 
                                                        <strong>${e.doc.host}</strong></div>` : ''}
                                                    ${e.doc.url ? `<a href="${e.doc.url}" class="row">
                                                        <img src="<?php echo plugin_dir_url(__DIR__) . 'images/transmission.png'; ?>" alt="site web">
                                                        Site Web
                                                        </a>` : ''}`)
                        marker.setPopup(popup) // sets a popup on this marker
                    }


                    marker.addTo(map);
                }
            })).catch(err => {
                alert(
                    "Il y a eu un problème dans le rapatriement des données : \n" + err
                );
            });

    <?php
    } else {
    ?>

        alert(
            "Il y a eu un problème dans le rapatriement des données : Constantes non définies"
        );
    <?php
    }
    ?>
</script>