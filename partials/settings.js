function deleteDocument(id, rev) {
    fetch(`${wpApiSettings.root}flyer-voyager/v1/delete-position/${id}?rev=${rev}&_wpnonce=${wpApiSettings.nonce}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
            }
        }).then((res) => {
            if (res.ok) {
                alert(
                    "Le point a bien été supprimé"
                );
                location.reload();
            } else {
                alert(
                    res.status +
                    ": Il y a eu un problème dans la suppression de la position"
                );
            }
        })
        .catch(err => {
            alert("Il y a eu un problème dans la suppression de la position : \n" + err)
        });
}