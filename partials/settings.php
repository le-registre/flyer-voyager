<?php
$url = 'https://' . constant("FVP_DB_URL") . '/flyer-voyager/_all_docs?include_docs=true';
$args = array(
    'headers' => array(
        'Authorization' => 'Basic ' . constant("FVP_DB_READER_KEY")
    )
);
$responses = wp_remote_get($url, $args);
$body = wp_remote_retrieve_body($responses);
$json_body = json_decode($body, true);
$rows = $json_body["rows"];

?>

<div class="wrap">
    <h1><?php echo esc_html(get_admin_page_title());?></h1>
    <table class="wp-list-table widefat fixed striped table-view-list pages">
        <thead>
            <tr>
                <th scope="col" id="namee" class="manage-column column-title">
                    <span>Nom</span><span class="sorting-indicator"></span>
                </th>
                <th scope="col" id="host" class="manage-column">Lieu d'accueil</th>
                <th scope="col" id="email" class="manage-column">E-Mail</th>
                <th scope="col" id="url" class="manage-column">Site Web</th>
                <th scope="col" id="coordinates" class="manage-column">Coordonnées</th>
                <th scope="col" id="date" class="manage-column">Date d'ajout</th>
                <th scope="col" id="delete" class="manage-column">Supprimer</th>
            </tr>
        </thead>
        <tbody id="the-list">
            <?php foreach ($rows as $row) : ?>
                <tr class="iedit level-0">
                    <td class="title">
                        <strong>
                            <p class="row-title"><?php echo $row["doc"]["name"] ?></p>
                        </strong>
                    </td>
                    <td>
                        <p><?php echo $row["doc"]["host"] ?></p>
                    </td>
                    <td>
                        <p><?php echo $row["doc"]["email"] ?></p>
                    </td>
                    <td>
                        <p><?php echo $row["doc"]["url"] ?></p>
                    </td>
                    <td>lat: <?php echo $row["doc"]["coord"][0] ?><br>lon: <?php echo $row["doc"]["coord"][1] ?></td>
                    <td>
                        <p><?php echo date_format(date_create($row["doc"]["timestamp"]), "d/m/Y"); ?></p>
                    </td>
                    <td>
                        <button onclick='deleteDocument(`<?php echo $row["doc"]["_id"] ?>`, `<?php echo $row["doc"]["_rev"] ?>`)'>Supprimer</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>